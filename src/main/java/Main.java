import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Scanner;
import java.util.stream.Stream;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static LocalDate local = LocalDate.now();

    public static void main(String[] args) throws IOException {
        notePadMenu();


    }

    public static void notePadMenu() throws IOException {

        int userChoice;
        boolean shouldContinue = true;
        while (shouldContinue) {
            System.out.println("--------NOTATNIK--------");
            System.out.println("1. Utworz nowa notatke");
            System.out.println("2. Usun notatke ");
            System.out.println("3. Edytuj  notatke");
            System.out.println("4. Wyswietl swoje notatki");
            System.out.println("5. Wyjdz ");
            userChoice = scanner.nextInt();
            switch (userChoice) {
                case 1 -> writeToFile(newNote());
                case 2 -> deleteFromFile();
                case 3 -> append();
                case 4 -> showNotes();
                case 5 -> {
                    System.out.println("Do zobaczenia");
                    shouldContinue = false;
                }
            }
        }
    }

    public static Textnote newNote() {
        String titel;
        String text;
        System.out.println("Podaj tytul notatki");
        titel = scanner.next();
        System.out.println("Podaj tresc notatki");
        text = scanner.next().concat(scanner.nextLine());
        return new Textnote(titel, text);

    }

    public static void writeToFile(Textnote textNote) throws IOException {
        File file = new File("src/main/" + textNote.getTitel() + ".txt");
        if (Files.exists(Path.of(textNote.getTitel() + ".txt"))) {
            System.out.println("Podan tytul notatki : << " + textNote.getTitel() + " >> juz istnieje");
        } else {
            Writer writer = new FileWriter(file.getName(), true);
            writer.write(local + "");
            writer.write("\n" + textNote.getText());
            System.out.println("Utworzono notatke  i zapisano do pliku: " + textNote.getTitel());
            writer.close();
        }
    }
    public static void deleteFromFile() throws IOException {
        System.out.println("Ktora notatke chcesz usunac ?");
        String titel = scanner.next();
        if (Files.exists(Path.of(titel + ".txt"))) {
            Files.delete(Path.of(titel + ".txt"));
            System.out.println("!Notatka zostala usunieta!");
        } else {
            System.out.println("Nazwa <<" + titel + ">> nie istnieje");
        }
    }
    public static void showNotes() {
        File f = new File("C:\\Users\\lulin\\IdeaProjects\\Notatnik");
        File[] files = f.listFiles();
        assert files != null;
        Stream<String> txtFiles = Stream.of(files).map(File::getName)
                .filter(s -> s.contains(".txt"));
        System.out.println(Arrays.stream(txtFiles.toArray()).toList());

    }
    public static void append() throws IOException {
        String titel ;
        System.out.println("Podaj tytul notatki");
        titel = scanner.next();
        if (Files.exists(Path.of(titel + ".txt"))) {
            File file = new File(titel + ".txt");
            Writer fileWriter = new FileWriter(file, true);
            Formatter formatter = new Formatter(fileWriter);
            System.out.println("Podaj tekst do dodania");
            String text = scanner.nextLine();
            formatter.format("\n" + scanner.nextLine());
            fileWriter.close();
            formatter.close();
        }else{
            System.out.println("Nie znaleziono notatki o podanym tytule");
        }
    }
}

