public record Textnote(String titel, String text) {

    public String getTitel() {
        return titel;
    }


    public String getText() {
        return text;
    }

}
